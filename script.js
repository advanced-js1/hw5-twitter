class Card {
	constructor(name, email, title, body, postId) {
		this.name = name;
		this.email = email;
		this.title = title;
		this.body = body;	
		this.postId = postId;
	}

	render() {
		const cardElement = document.createElement('div');
			cardElement.classList.add('card');
			cardElement.innerHTML = `
				<div class="user">
					<h3>${this.name}</h3>
					<a href="mailto:${this.email}">${this.email}</a>
				</div>				
				<h3>${this.title}</h3>
				<p>${this.body}</p>
				<button class="card-button">Delete</button>
			`;
		const btnDelete = cardElement.querySelector('.card-button');
		btnDelete.addEventListener('click', () => {
			this.deleteCard();
		});

		return cardElement;
	}
	
	async deleteCard() {
		try {
			const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
				method: 'DELETE'
			});

			if (response.ok) {
				const cardElement = document.querySelector(`[data-post-id="${this.postId}"]`);
				cardElement.remove();
			} else {
				console.error('Error:', response.status);
			}
		} catch (error) {
			console.error('Error:', error);
		}
	}
}

async function getUsers() {
	let response = await fetch("https://ajax.test-danit.com/api/json/users");
	let data = await response.json();
	return data;
}

async function getPosts() {
	let response = await fetch("https://ajax.test-danit.com/api/json/posts");
	let data = await response.json();
	return data;
}

async function viewCards() {
	const users = await getUsers();
	const posts = await getPosts();

	const postContainer = document.querySelector('.posts-container');
	postContainer.innerHTML = '';

	posts.forEach(post => {
		const user = users.find(user => user.id === post.userId);

		const card = new Card(user.name, user.email, post.title, post.body, post.id );
		const cardElement = card.render();
		cardElement.setAttribute('data-post-id', post.id);
		postContainer.append(cardElement);
	});
}

viewCards();

